package hu.user.icecreamwizard.sandbox;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class TestBean {

	private PropertyChangeSupport pcs = null;
	private String prop1 = null;
	
	public TestBean() {
		pcs = new PropertyChangeSupport(this);
	}
	
	public String getProp1() {
		return prop1;
	}
	
	public void setProp1(String _prop1) {
		this.pcs.firePropertyChange("prop1", prop1, _prop1);
		this.prop1 = _prop1;
	}
	
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.removePropertyChangeListener(listener);
	}
	
}

