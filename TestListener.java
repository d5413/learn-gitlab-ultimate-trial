package hu.user.icecreamwizard.sandbox;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class TestListener implements PropertyChangeListener{

	
	public TestListener() {}
	
	/*public static void main(String[] args) {
		new TestListener();
	}*/
	
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		System.out.println(evt);
	}
	
	//Vagy apache beanutils segitsegevel
	//public class BeanWrapper{
	//	static public void setProperty("propertyName", Object beanObject, Object newValue){
	//		Object oldValue = BeanUtils.getProperty("propertyName", beanObject);
	//		beanObject.getPropertyChangeSupport().firePropertyChange("propertyName", oldValue, newValue);
	//		BeanUtils.setProperty("propertyName", beanObject);
	//	}
	//
	
}
