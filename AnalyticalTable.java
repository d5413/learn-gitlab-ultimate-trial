package hu.user.icecreamwizard.analyticaltable;

import java.util.ArrayList;
import java.util.List;

import hu.user.icecreamwizard.model.nutrients.Nutrient;
import hu.user.icecreamwizard.model.nutrients.Sugar;
import hu.user.icecreamwizard.model.nutrients.Water;
import hu.user.icecreamwizard.model.recipe.Recipe;
import hu.user.icecreamwizard.model.recipe.RecipeChangeListener;
import hu.user.icecreamwizard.model.recipe.RecipeIngredient;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class AnalyticalTable {

	private Recipe recipe;
	private AnalyticalTableRenderer renderer;
	private List<Column> columns;
	private double sucroseEquivalent;
	private double fpdSE;
	private double fpdSA;
	private double fpdTotal;
	private Cell cells[][];
	private HeaderCell[] headerCells;
	private TotalCell[] totalCells;
	private RecipeChangeListener rc = new RecipeChangeListener();

	public AnalyticalTable(Recipe recipe) {
		this.recipe = recipe;
		this.columns = new ArrayList<Column>();
		calculateFreezingPointDepression();
	}

	public void setRenderer(AnalyticalTableRenderer renderer) {
		this.renderer = renderer;
		this.renderer.setTable(this);
	}
	
	private void calculateFreezingPointDepression() {
		double sucroseConcentration[] = {
				 0,
				 3,   6,   9,  12,  15,  18,  21,  24,  27,  30, 
			    33,  36,  39,  42,  45,  48,  51,  54,  57,  60,
			    63,  66,  69,  72,  75,  78,  81,  84,  87,  90,
			    93,  96,  99, 102, 105, 108, 111, 114, 117, 120,
			   123, 126, 129, 132, 135, 138, 141, 144, 147, 150,
			   153, 156, 159, 162, 165, 168, 171, 174, 177, 180,
		};
		
		double fpd[] = {
				 0,
				 0.18,  0.35,  0.53,  0.73,  0.90,   1.10,  1.29,  1.47,  1.67,  1.86,
				 2.03,  2.21,  2.40,  2.60,  2.78,   2.99,  3.20,  3.42,  3.63,  3.85,
				 4.10,  4.33,  4.54,  4.77,  5.00,   5.26,  5.53,  5.77,  5.99,  6.23,
				 6.50,  6.80,  7.04,  7.32,  7.56,   7.80,  8.04,  8.33,  8.62,  8.92,
				 9.19,  9.45,  9.71,  9.96, 10.22,  10.47, 10.72, 10.97, 11.19, 11.41,
				11.63, 11.88, 12.14, 12.40, 12.67,  12.88, 13.08, 13.28, 13.48, 13.68
		};
		
		fpdSA = 0;
		sucroseEquivalent = calculateSE();
		double totalWater = calculateTotalWater();
		double normalizedSucroseEquivalent = sucroseEquivalent / totalWater * 100;
		
//		System.out.println(sucroseEquivalent);
//		System.out.println(totalWater);
//		System.out.println(normalizedSucroseEquivalent);

		int i = 0;
		for( i = 0; i < sucroseConcentration.length-1; i++ ) {
			if( (normalizedSucroseEquivalent >= sucroseConcentration[i]) &&
				(normalizedSucroseEquivalent < sucroseConcentration[i+1])
			) {
//				System.out.println(fpd[i] + "," + fpd[i+1]);
				break;
			}
		}
		
		fpdSE = fpd[i] + (fpd[i+1]-fpd[i]) / (sucroseConcentration[i+1]-sucroseConcentration[i])*(normalizedSucroseEquivalent-sucroseConcentration[i]);
		fpdTotal = fpdSA + fpdSE;
	}

	public Object render() {
		computeHeaderCells();
		computeCells();
		computeTotalCells();
		return renderer.render();
	}

	private void computeHeaderCells() {		
		headerCells = new HeaderCell[columns.size()];
		int col = 0;
		for( Column column : columns ) {
			headerCells[col++] = column.getHeaderCell();
		}
	}
	
	private void computeCells() {
		int rows = recipe.getIngredients().size();
		int cols = columns.size();
		cells = new Cell[rows][cols];
		
		int col = 0;
		int row = 0;
		for( RecipeIngredient ingredient : recipe.getIngredients() ) {
			col = 0;
			for( Column column : columns ) {
				cells[row][col++] = column.getCell(ingredient);
			}
			row++;
		}
	}

	private void computeTotalCells() {
		int cols = columns.size();
		totalCells = new TotalCell[cols];
		
		int col = 0;
		for( Column column : columns ) {
			totalCells[col++] = column.getTotalCell();
		}
	}
	
	public Recipe getRecipe() {
		return recipe;
	}

	public Column[] getColumns() {
		return columns.toArray(new Column[] {});
	}

	public void addColumn(Column analyticalTableColumn) {
		columns.add(analyticalTableColumn);
	}

	public double getTotalWeight() {
		double total = 0;
		for( RecipeIngredient ri : recipe.getIngredients() ) {
			total += ri.getAmount();
			ri.addPropertyChangeListener(rc);
			ri.setAmount(ri.getAmount());
		}
		return total;
	}
	
	private double calculateSE() {
		double se = 0;
		for( RecipeIngredient i : recipe.getIngredients() ) {
			for( Nutrient n : i.getIngredient().getNutrients() ) {
				if( n instanceof Sugar ) {
					se += ((Sugar)n).getSE() * (i.getAmount() * n.getAmount() / 100);
				}
			}
		}
		return se;
	}

	private double calculateTotalWater() {
		double totalWater = 0;
		for( RecipeIngredient i : recipe.getIngredients() ) {
			for( Nutrient n : i.getIngredient().getNutrients() ) {
				if( n instanceof Water ) {
					totalWater += i.getAmount() * n.getAmount() / 100;
				}
			}
		}
		return totalWater;
	}

	public double getSucroseEquivalent() {
		return sucroseEquivalent;
	}
	
	public double getFPDSA() {
		return fpdSA;
	}
	
	public double getFPDSE() {
		return fpdSE;
	}
	
	public double getFPD() {
		return fpdTotal;
	}

	public Cell[][] getCells() {
		return cells;
	}
	
	public HeaderCell[] getHeaderCells() {
		return headerCells;
	}

	public TotalCell[] getTotalCells() {
		return totalCells;
	}
}