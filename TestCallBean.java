package hu.user.icecreamwizard.sandbox;

public class TestCallBean {
	public static void main(String[] args) {
		TestBean tb = new TestBean();
		TestListener tl = new TestListener();
		tb.addPropertyChangeListener(tl);
		tb.setProp1(String.valueOf(System.currentTimeMillis()));
	}
}
