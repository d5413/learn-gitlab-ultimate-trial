package hu.user.icecreamwizard.model.recipe;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

@Entity
@Table(name = "recipeingredient")
public class RecipeIngredient {
	
	
	@Id
	@GeneratedValue
	private long id;

	//@Transient
	private PropertyChangeSupport pcs = null;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade={CascadeType.PERSIST,CascadeType.REFRESH,CascadeType.MERGE})
	private Ingredient ingredient = null;
	private double amount = 0;
	
	public RecipeIngredient() {}
	
	public RecipeIngredient(Ingredient ingredient, double amount) {
		this.ingredient = ingredient;
		this.amount = amount;
		pcs = new PropertyChangeSupport(this);
	}
	
	public Ingredient getIngredient() {
		return ingredient;
	}
	
	public double getAmount() {
		return amount;
	}
	
	public Object visit(IVisitor visitor, Object object) {
		return visitor.visitRecipeIngedient(this, object);
	}

	public void setAmount(double _amount) {
		this.pcs.firePropertyChange("amount", this.amount, amount);
		amount = _amount;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.removePropertyChangeListener(listener);
	}
}
